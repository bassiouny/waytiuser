package Bean;

/**
 * Created by Ayaomar on 7/17/2017.
 */

public class RequestBean {

    String driverID;
    String userID;
    int status;
    int isRejected;

    public RequestBean() {
    }

    public RequestBean(String driverID, String userID, int status , int isRejected) {
        this.driverID = driverID;
        this.userID = userID;
        this.status = status;
    }

    public String getDriverID() {
        return driverID;
    }

    public void setDriverID(String driverID) {
        this.driverID = driverID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getIsRejected() {
        return isRejected;
    }

    public void setIsRejected(int isRejected) {
        this.isRejected = isRejected;
    }
}
