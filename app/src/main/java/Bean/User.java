package Bean;

/**
 * Created by Ayaomar on 6/4/2017.
 */

public class User {

    String id;
    String name;
    double latitude;
    double longitude;
    String mobile;
    String photo;
    String city;
    Boolean isActive;
    Boolean block;
    String password;

    public User() {
    }


    public User(String id, String name, double latitude, double longitude, String mobile, String photo,String city ,Boolean isActive ,
                Boolean block) {
        this.id = id;
        this.name = name;

        this.latitude = latitude;
        this.longitude = longitude;
        this.mobile = mobile;
        this.photo = photo;
        this.city = city;
        this.isActive = isActive;
        this.block = block;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean active) {
        isActive = active;
    }

    public Boolean getBlock() {
        return block;
    }

    public void setBlock(Boolean block) {
        this.block = block;
    }

    public String getPassword() {
        if(password==null)
            password="";
        return password;
    }
}
