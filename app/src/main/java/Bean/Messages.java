package Bean;

/**
 * Created by Ayaom on 7/2/2017.
 */

public class Messages {

    String userSeen;
    String isSender;
    String message;
    String time_date;

    public Messages(String userSeen,String isSender, String message, String time_date) {
        this.userSeen = userSeen;
        this.isSender = isSender;
        this.message = message;
        this.time_date = time_date;
    }
    public Messages(){
    }

    public String getIsSender() {
        return isSender;
    }

    public void setIsSender(String isSender) {
        this.isSender = isSender;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTime_date() {
        return time_date;
    }

    public void setTime_date(String time_date) {
        this.time_date = time_date;
    }

    public String getUserSeen() {
        return userSeen;
    }

    public void setUserSeen(String unseen) {
        this.userSeen = unseen;
    }
}
