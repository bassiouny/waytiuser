package Bean;

/**
 * Created by Ayaom on 6/29/2017.
 */

public class chatBean {

    String img;
    String name;
    String msg;
    String noOfUnseen;
    String receiverID;

    public chatBean(String receiverID, String img, String name, String msg, String noOfUnseen) {
        this.img = img;
        this.name = name;
        this.msg = msg;
        this.noOfUnseen = noOfUnseen;
        this.receiverID = receiverID;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getNoOfUnseen() {
        return noOfUnseen;
    }

    public void setNoOfUnseen(String noOfUnseen) {
        this.noOfUnseen = noOfUnseen;
    }

    public String getReceiverID() {
        return receiverID;
    }

    public void setReceiverID(String receiverID) {
        this.receiverID = receiverID;
    }
}
