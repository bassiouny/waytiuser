package Bean;

/**
 * Created by Ayaom on 7/10/2017.
 */

public class DriverBean {

    String name;
    double location_latitude;
    double location_longitude;
    boolean isDrinkable;
    String car_img1;
    String mobile;
    int weight;
    int total_rate;
    int total_no_of_rate;
    String photo;
    Boolean exit;
    private double price=1.0;
    public String token="";
    public DriverBean() {
    }

    public DriverBean(String photo, String name, double location_latitude, double location_longitude, boolean isDrinkable, String car_img1, String mobile, int weight, int total_rate, int total_no_of_rate
    ,Boolean exit) {
        this.photo = photo;
        this.name = name;
        this.location_latitude = location_latitude;
        this.location_longitude = location_longitude;
        this.isDrinkable = isDrinkable;
        this.car_img1 = car_img1;
        this.mobile = mobile;
        this.weight = weight;
        this.total_rate = total_rate;
        this.total_no_of_rate = total_no_of_rate;
        this.exit = exit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLocation_latitude() {
        return location_latitude;
    }

    public void setLocation_latitude(double location_latitude) {
        this.location_latitude = location_latitude;
    }

    public double getLocation_longitude() {
        return location_longitude;
    }

    public void setLocation_longitude(double location_longitude) {
        this.location_longitude = location_longitude;
    }

    public boolean getIsDrinkable() {
        return isDrinkable;
    }

    public void setIsDrinkable(Boolean drinkable) {
        isDrinkable = drinkable;
    }

    public String getCar_img1() {
        return car_img1;
    }

    public void setCar_img1(String car_img1) {
        this.car_img1 = car_img1;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getTotal_rate() {
        return total_rate;
    }

    public void setTotal_rate(int total_rate) {
        this.total_rate = total_rate;
    }

    public int getTotal_no_of_rate() {
        return total_no_of_rate;
    }

    public void setTotal_no_of_rate(int total_no_of_rate) {
        this.total_no_of_rate = total_no_of_rate;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Boolean getExit() {
        return exit;
    }

    public void setExit(Boolean exit) {
        this.exit = exit;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
