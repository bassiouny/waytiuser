package Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.ntamtech.waytiuser.R;

import java.util.ArrayList;

import Bean.chatBean;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Ayaomar on 5/2/2017.
 */

public class chatAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflater;
    ArrayList<chatBean> chatBeen;
    RelativeLayout relativeLayout;
    private DatabaseReference mDatabase;


    public chatAdapter(Context context, ArrayList<chatBean> chatBeen) {
        this.context = context;
        this.chatBeen = chatBeen;
        inflater = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return chatBeen.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        SharedPreferences preferences = context.getSharedPreferences("language", MODE_PRIVATE);
        String lang = preferences.getString("lang", "");
        if (lang.equals("ar")){
            view = inflater.inflate(R.layout.custom_chat_row, null);
        }else {
            view = inflater.inflate(R.layout.custom_chat_row_left, null);
        }
        TextView nameTV = (TextView) view.findViewById(R.id.name);
        TextView msgTV = (TextView) view.findViewById(R.id.msg);
        relativeLayout = (RelativeLayout)view.findViewById(R.id.relativeLayout);
//        TextView noOfUnseenTV = (TextView) view.findViewById(R.id.noOfUnseen);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        ImageView profileImgTV = (ImageView)view.findViewById(R.id.profile_image);

        nameTV.setText(chatBeen.get(i).getName());
        msgTV.setText(chatBeen.get(i).getMsg());
      //  noOfUnseenTV.setText(chatBeen.get(i).getNoOfUnseen());

        Glide.with(context).load(chatBeen.get(i).getImg())
                .into(profileImgTV);

        if (chatBeen.get(i).getNoOfUnseen().equals("0")){
            relativeLayout.setBackgroundColor(context.getResources().getColor(R.color.unseen));
        }else {
            relativeLayout.setBackgroundColor(context.getResources().getColor(R.color.white));
        }

        view.setTag(chatBeen.get(i).getReceiverID());

        return view;
    }
}