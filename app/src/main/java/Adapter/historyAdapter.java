package Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.ntamtech.waytiuser.R;

import java.util.ArrayList;
import Bean.historyBean;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Ayaomar on 5/2/2017.
 */

public class historyAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflater;
    ArrayList<historyBean> historyBeen;


    public historyAdapter(Context context, ArrayList<historyBean> historyBeen) {
        this.context = context;
        this.historyBeen = historyBeen;
        inflater = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return historyBeen.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        SharedPreferences preferences = context.getSharedPreferences("language", MODE_PRIVATE);
        String lang = preferences.getString("lang", "");
        if (lang.equals("ar")){
            view = inflater.inflate(R.layout.custom_cell_history, null);
        }else {
            view = inflater.inflate(R.layout.custom_cell_history_left, null);
        }
        TextView nameTV = (TextView) view.findViewById(R.id.name);
        TextView phoneTV = (TextView) view.findViewById(R.id.phone);
        TextView dateTV = (TextView) view.findViewById(R.id.date);
        ImageView profileImgTV = (ImageView)view.findViewById(R.id.profile_image);

        nameTV.setText(historyBeen.get(i).getUsername());
        phoneTV.setText(historyBeen.get(i).getUser_phone());
        dateTV.setText(historyBeen.get(i).getDate());

        Glide.with(context).load(historyBeen.get(i).getUser_photo())
                .into(profileImgTV);

        return view;
    }
}