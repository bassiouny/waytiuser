package Config;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by Ayaom on 8/2/2017.
 */

public interface RestInterface {

    String baseURL = "http://ntam.tech/send_sms";

    @FormUrlEncoded
    @POST("/send_sms.php")
    void editPassword(@Field("phone") String phone,
                      @Field("code") int code,
                      Callback<String> serverResponseCallback);

    @FormUrlEncoded
    @POST("")
    void sendToken(@Field("title") String title,
                   @Field("body") String body,
                   @Field("token_id") String token_id,
                   Callback<String> serverResponseCallback);


}
