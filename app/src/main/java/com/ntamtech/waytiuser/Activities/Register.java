package com.ntamtech.waytiuser.Activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.ntamtech.waytiuser.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import Util.Utility;

public class Register extends AppCompatActivity {

    Button nextBtn;
    ImageView backIV;
    ImageView profileIV;
    EditText nameET;
    EditText phoneET;
    EditText passwordET;
    EditText placeET;
    String userChoosenTask;
    private int REQUEST_CAMERA = 0;
    private int SELECT_FILE = 1;
    private CoordinatorLayout coordinatorLayout;
    Uri filePath = Uri.parse("");
    StorageReference storageRef;
    String imgName;
    Bitmap bitmap;
    private FirebaseAuth auth;
    private ProgressDialog dialog;
    Spinner citySP;
    String address;
    String phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        init();
        events();

    }

    public void init() {

        nextBtn = (Button) findViewById(R.id.next);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        nextBtn = (Button) findViewById(R.id.next);
        backIV = (ImageView) findViewById(R.id.back);
        profileIV = (ImageView) findViewById(R.id.profileIV);
        nameET = (EditText) findViewById(R.id.name);
        phoneET = (EditText) findViewById(R.id.phone);
        placeET = (EditText) findViewById(R.id.location);
        passwordET = (EditText) findViewById(R.id.password);
        citySP = (Spinner) findViewById(R.id.city);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        storageRef = FirebaseStorage.getInstance().getReference().child("/images");
        auth = FirebaseAuth.getInstance();
        dialog = new ProgressDialog(this);
        dialog.setMessage("Please wait...");
        dialog.setCancelable(false);

        SharedPreferences preferences = getSharedPreferences("userData", MODE_PRIVATE);
        address = preferences.getString("address", "");
        placeET.setText(address);

    }

    public void events() {
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
                final String name = nameET.getText().toString().trim();
                phone = phoneET.getText().toString().trim();
                final String place = placeET.getText().toString().trim();
                final String password = passwordET.getText().toString().trim();

                if (!Utility.isNotNull(filePath.toString())){
                    SharedPreferences.Editor editor = getSharedPreferences("userData", MODE_PRIVATE).edit();
                    editor.putString("profileImg", " ");
                    editor.commit();
                }
                if (phone.length()==9) {
                    if (Utility.isNotNull(name) && Utility.isNotNull(phone) && Utility.isNotNull(password) && Utility.isNotNull(address)) {

                        if (password.length() < 6) {
                            Snackbar snackbar = Snackbar.make(coordinatorLayout, "password length must be over 6", Snackbar.LENGTH_SHORT);
                            snackbar.show();
                            dialog.dismiss();
                        } else {
                            Log.i("kkk",phone);
                            auth.createUserWithEmailAndPassword(phone + "@gmail.com", password)
                                    .addOnCompleteListener(Register.this, new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {
                                            //    Toast.makeText(Registeration.this, "createUserWithEmail:onComplete:" + task.isSuccessful(), Toast.LENGTH_SHORT).show();
                                            if (!task.isSuccessful()) {
                                                dialog.hide();
                                                String msg = task.getException().getMessage().replace("email", "phone");
                                                String message = msg.replace("address", "");
                                                Toast.makeText(Register.this, message, Toast.LENGTH_SHORT).show();
                                            } else {
                                                imgName = task.getResult().getUser().getUid();
                                                SharedPreferences.Editor editor = getSharedPreferences("userData", MODE_PRIVATE).edit();
                                                editor.putString("id", task.getResult().getUser().getUid());
                                                editor.putString("name", name);
                                                editor.putString("phone", phone);
                                                editor.putString("city", citySP.getSelectedItem().toString());
                                                editor.putString("password", password);
                                                editor.commit();
                                                if (Utility.isNotNull(filePath.toString())) {
                                                    addData(filePath);
                                                }else {
                                                    dialog.dismiss();
                                                    final Intent mainIntent = new Intent(Register.this, Pledge.class);
                                                    startActivity(mainIntent);
                                                }
                                            }
                                        }
                                    });
                        }
                    } else {
                        dialog.hide();
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.fillData), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    dialog.hide();
                    final AlertDialog alertDialog = new AlertDialog.Builder(Register.this).create();
                    alertDialog.setMessage(getString(R.string.format_num));
                    alertDialog.setCancelable(false);
                    alertDialog.setButton(Dialog.BUTTON_POSITIVE,getString(R.string.ok),new DialogInterface.OnClickListener(){
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            alertDialog.dismiss();
                        }
                    });
                    alertDialog.show();
                }

            }
        });

        placeET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent mainIntent = new Intent(Register.this, SetLocation.class);
                startActivity(mainIntent);
            }
        });

        backIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        profileIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
    }

    //===============================================

    private void selectImage() {
        final CharSequence[] items = {getString(R.string.take_photo), getString(R.string.choose_from_library), getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(Register.this);
        builder.setTitle(R.string.addPhoto);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(Register.this);
                if (items[item].equals(getString(R.string.take_photo))) {
                    userChoosenTask = getString(R.string.take_photo);
                    if (result)
                        cameraIntent();
                } else if (items[item].equals(getString(R.string.choose_from_library))) {
                    userChoosenTask = getString(R.string.choose_from_library);
                    if (result)
                        galleryIntent();
                } else if (items[item].equals(getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals(getString(R.string.take_photo)))
                        cameraIntent();
                    else if (userChoosenTask.equals(getString(R.string.choose_from_library)))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                onSelectFromGalleryResult(data);
                filePath = data.getData();
                profileIV.setImageBitmap(bitmap);

            } else if (requestCode == REQUEST_CAMERA) {
                onCaptureImageResult(data);
                filePath = data.getData();
                profileIV.setImageBitmap(bitmap);
            }
        }
    }

    private void onSelectFromGalleryResult(Intent data) {

        if (data != null) {
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void onCaptureImageResult(Intent data) {
        bitmap = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
//==================================================

    public void addData(Uri filePath) {
        if (filePath != null) {
            StorageReference childRef = storageRef.child(imgName);
            UploadTask uploadTask = childRef.putFile(filePath);
            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//                    pd.dismiss();
                    @SuppressWarnings("VisibleForTests") Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    SharedPreferences.Editor editor = getSharedPreferences("userData", MODE_PRIVATE).edit();
                    editor.putString("profileImg", downloadUrl.toString());
                    editor.commit();
                    Toast.makeText(Register.this, "Image Uploaded successfully", Toast.LENGTH_SHORT).show();
                    dialog.hide();
                    final Intent mainIntent = new Intent(Register.this, Pledge.class);
                    startActivity(mainIntent);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
//                    pd.dismiss();
                    dialog.hide();
                    Toast.makeText(Register.this, " Failed to Upload img ", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(Register.this, "Select an image", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences preferences = getSharedPreferences("userData", MODE_PRIVATE);
        address = preferences.getString("address", "");
        placeET.setText(address);

    }

    public void setLocation() {
        final Intent mainIntent = new Intent(Register.this, SetLocation.class);
        startActivity(mainIntent);

    }
}
