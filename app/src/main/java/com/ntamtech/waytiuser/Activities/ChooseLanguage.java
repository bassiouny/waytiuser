package com.ntamtech.waytiuser.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ntamtech.waytiuser.R;

import java.util.Locale;

import Config.LANG;

public class ChooseLanguage extends AppCompatActivity {
    TextView chooseTV;
    RelativeLayout arabicLanguage;
    RelativeLayout englishLanguage;
    TextView driverTV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_language);

        init();
        events();
    }

    public void init() {
        chooseTV = (TextView) findViewById(R.id.choose);
        arabicLanguage = (RelativeLayout)findViewById(R.id.arabicLanguage);
        englishLanguage = (RelativeLayout)findViewById(R.id.english);
        driverTV = (TextView)findViewById(R.id.driver);
    }

    public void events() {
        arabicLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetLanguage(getApplicationContext(), LANG.ARABIC);
                final Intent mainIntent = new Intent(ChooseLanguage.this, Login.class);
                startActivity(mainIntent);
                finish();
                SharedPreferences.Editor editor = getSharedPreferences("language", MODE_PRIVATE).edit();
                editor.putString("lang", "ar");
                editor.commit();
            }
        });

        englishLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetLanguage(getApplicationContext(), LANG.ENGLISH);
                final Intent mainIntent = new Intent(ChooseLanguage.this, Login.class);
                startActivity(mainIntent);
                finish();

                SharedPreferences.Editor editor = getSharedPreferences("language", MODE_PRIVATE).edit();
                editor.putString("lang", "en");
                editor.commit();
            }
        });

        driverTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://play.google.com/store/apps/details?id=com.ntamtech.wayti";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
    }

    public static void SetLanguage(Context ctx, LANG selectedLang) {
        if (selectedLang == LANG.ENGLISH) {
            // Change layout to en
            Locale mLocale = new Locale("en");
            Locale.setDefault(mLocale);
            Configuration config = ctx.getResources().getConfiguration();

            if (!config.locale.equals(mLocale)) {
                config.locale = mLocale;
                ctx.getResources().updateConfiguration(config, null);
            }
//            SharedPrefCustom.saveLocalization(ctx, english);
        } else {
            // Change layout to ar
            Locale mLocale = new Locale("ar");
            Locale.setDefault(mLocale);
            Configuration config = ctx.getResources().getConfiguration();

            if (!config.locale.equals(mLocale)) {
                config.locale = mLocale;
                ctx.getResources().updateConfiguration(config, null);
            }
//            SharedPrefCustom.saveLocalization(ctx, arabic);
        }
    }

}
