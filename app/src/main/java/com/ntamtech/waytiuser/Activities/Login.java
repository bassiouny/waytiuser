package com.ntamtech.waytiuser.Activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ntamtech.waytiuser.R;
import com.twilio.client.Connection;
import com.twilio.client.ConnectionListener;
import com.twilio.client.Device;
import com.twilio.client.DeviceListener;
import com.twilio.client.PresenceEvent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import Bean.User;
import Config.SaveSharedPreference;
import Util.Utility;

public class Login extends AppCompatActivity implements DeviceListener, ConnectionListener {
    TextView register;
    TextView forgetPasswordTV;
    Button loginBtn;
    EditText phoneET, passwordET;
    private FirebaseAuth auth;
    private DatabaseReference mDatabase;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if (!SaveSharedPreference.getUserName(Login.this).equals("")) {
            Intent intent = new Intent(getApplicationContext(), Home.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        } else {

        }

        init();
        events();
    }

    public void init() {

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        forgetPasswordTV = (TextView) findViewById(R.id.forgetPass);
        register = (TextView) findViewById(R.id.register);
        loginBtn = (Button) findViewById(R.id.login);
        phoneET = (EditText) findViewById(R.id.phone);
        passwordET = (EditText) findViewById(R.id.password);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        auth = FirebaseAuth.getInstance();
        dialog = new ProgressDialog(this);
        dialog.setMessage("Please wait...");
        dialog.setCancelable(false);

    }

    public void events() {

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent mainIntent = new Intent(Login.this, Register.class);
                startActivity(mainIntent);
            }
        });

        forgetPasswordTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login.this,ForgetPasswordActivity.class));

            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();

                loginUser(phoneET.getText().toString(), passwordET.getText().toString());
            }
        });
    }

    private void loginUser(String phone, final String password) {


        if (phone.length() == 9) {

            if (Utility.isNotNull(phone) && Utility.isNotNull(password)) {
                phone = phone + "@gmail.com";
                auth.signInWithEmailAndPassword(phone, password)
                        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull final Task<AuthResult> task) {
                                if (!task.isSuccessful()) {
                                    if (password.length() < 6) {
//                                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "password length must be over 6", Snackbar.LENGTH_SHORT);
//                                    snackbar.show();
                                        Toast.makeText(getApplicationContext(), "Password length must be over 6", Toast.LENGTH_SHORT).show();

                                    }
                                    dialog.hide();
                                    Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();

                                } else {
                                    mDatabase.getRoot().child("user").addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            if (dataSnapshot.hasChild(task.getResult().getUser().getUid())) {
                                                mDatabase.getRoot().child("user").child(task.getResult().getUser().getUid()).addValueEventListener(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(DataSnapshot dataSnapshot) {

                                                        if (!dataSnapshot.getValue(User.class).getBlock()) {
                                                            String id = task.getResult().getUser().getUid();
                                                            String name = dataSnapshot.getValue(User.class).getName();
                                                            double location_latitude = dataSnapshot.getValue(User.class).getLatitude();
                                                            double location_longitude = dataSnapshot.getValue(User.class).getLongitude();
                                                            String mobile = dataSnapshot.getValue(User.class).getMobile();
                                                            String photo = dataSnapshot.getValue(User.class).getPhoto();
                                                            String city = dataSnapshot.getValue(User.class).getCity();

                                                            Boolean isActive = dataSnapshot.getValue(User.class).getIsActive();
                                                            SaveSharedPreference.setUserName(getApplicationContext(), mobile);

                                                            SharedPreferences.Editor editor = getSharedPreferences("userData", MODE_PRIVATE).edit();
                                                            editor.putString("id", id);
                                                            editor.putString("name", name);
                                                            editor.putString("phone", mobile);
                                                            editor.putString("password", password);
                                                            editor.putString("profileImg", photo);
                                                            editor.putString("city", city);
                                                            editor.putString("lng", location_longitude + "");
                                                            editor.putString("lat", location_latitude + "");
                                                            editor.commit();
                                                            dialog.hide();
                                                            if (!SaveSharedPreference.getUserName(Login.this).equals("")) {

                                                                if (isActive == true) {
                                                                    startActivity(new Intent(Login.this, Home.class));
                                                                } else {
                                                                    startActivity(new Intent(Login.this, Activation.class));
                                                                }
                                                            }
                                                        } else {
                                                            dialog.dismiss();
                                                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.blok_msg), Toast.LENGTH_SHORT).show();
                                                        }

                                                    }

                                                    @Override
                                                    public void onCancelled(DatabaseError databaseError) {
                                                        dialog.hide();
                                                    }
                                                });
                                            } else {
                                                dialog.hide();
                                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.userNotFound), Toast.LENGTH_SHORT).show();
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                            dialog.hide();
                                        }
                                    });
                                }
                            }
                        });
            } else {
                dialog.hide();
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.fillData), Toast.LENGTH_SHORT).show();
            }
        } else {
            dialog.hide();
            final AlertDialog alertDialog = new AlertDialog.Builder(Login.this).create();
            alertDialog.setMessage(getString(R.string.format_num));
            alertDialog.setCancelable(false);
            alertDialog.setButton(Dialog.BUTTON_POSITIVE, getString(R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    alertDialog.dismiss();
                }
            });
            alertDialog.show();
        }
    }

    @Override
    public void onConnecting(Connection connection) {

    }

    @Override
    public void onConnected(Connection connection) {

    }

    @Override
    public void onDisconnected(Connection connection) {

    }

    @Override
    public void onDisconnected(Connection connection, int i, String s) {

    }

    @Override
    public void onStartListening(Device device) {

    }

    @Override
    public void onStopListening(Device device) {

    }

    @Override
    public void onStopListening(Device device, int i, String s) {

    }

    @Override
    public boolean receivePresenceEvents(Device device) {
        return false;
    }

    @Override
    public void onPresenceChanged(Device device, PresenceEvent presenceEvent) {

    }
}
