package com.ntamtech.waytiuser.Activities;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.ntamtech.waytiuser.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import Util.Utility;

public class Complains extends AppCompatActivity {

    EditText nameET;
    EditText phoneET;
    EditText complainET;
    private DatabaseReference mDatabase;
    Button sendBtn;
    private ProgressDialog dialog;
    ImageView backIV;
    ImageView backLeftIV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complains);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        intail();
        events();

        SharedPreferences sharedPreferences = getSharedPreferences("userData", MODE_PRIVATE);
        final String phone = sharedPreferences.getString("phone", "");
        final String name = sharedPreferences.getString("name", "");
        phoneET.setText(phone+"");
        nameET.setText(name);

    }

    public void intail() {

        phoneET = (EditText) findViewById(R.id.phone);
        nameET = (EditText) findViewById(R.id.name);
        complainET = (EditText) findViewById(R.id.complain);
        sendBtn = (Button) findViewById(R.id.send);
        backIV = (ImageView)findViewById(R.id.back);
        backLeftIV = (ImageView)findViewById(R.id.backLeft);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        dialog = new ProgressDialog(this);
        dialog.setMessage("Please wait...");
        dialog.setCancelable(false);

        SharedPreferences preferences = getSharedPreferences("language", MODE_PRIVATE);
        String lang = preferences.getString("lang", "");
        if (lang.equals("ar")){
            backLeftIV.setVisibility(View.GONE);
        }else {
            backIV.setVisibility(View.GONE);
        }
    }

    public void events() {
        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.show();
                String complain = complainET.getText().toString();
                String name = nameET.getText().toString();
                String phone = phoneET.getText().toString();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                String currentDateandTime = sdf.format(new Date());

                if (Utility.isNotNull(phoneET.getText().toString()) && Utility.isNotNull(nameET.getText().toString())
                        && Utility.isNotNull(complainET.getText().toString())) {

                    Map<String, Object> complainData = new HashMap<String, Object>();
                    complainData.put("description", complain);
                    complainData.put("phone", phone);
                    complainData.put("time", currentDateandTime);
                    complainData.put("username", name);
                    mDatabase.child("complains").push().setValue(complainData);
                    finish();
                    Toast.makeText(getApplicationContext(), "sent successfully", Toast.LENGTH_SHORT).show();
                } else {

                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.fillData), Toast.LENGTH_SHORT).show();
                    dialog.hide();
                }
            }
        });

        backIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        backLeftIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {finish();}});
    }

}
