package com.ntamtech.waytiuser.Activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ntamtech.waytiuser.R;
import java.util.ArrayList;
import java.util.Map;
import Adapter.historyAdapter;
import Bean.historyBean;

public class History extends AppCompatActivity {

    historyAdapter adapter;
    ListView lv;
    ArrayList<historyBean> historyBeens = new ArrayList<>();
    private DatabaseReference mDatabase;
    ImageView backIV;
    ImageView backLeftIV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        lv = (ListView) findViewById(R.id.lv);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        backIV = (ImageView) findViewById(R.id.back);
        backLeftIV = (ImageView)findViewById(R.id.backLeft);


        SharedPreferences sharedPreferences = getSharedPreferences("language", MODE_PRIVATE);
        String lang = sharedPreferences.getString("lang", "");
        if (lang.equals("ar")){
            backLeftIV.setVisibility(View.GONE);
        }else {
            backIV.setVisibility(View.GONE);
        }

        SharedPreferences preferences = getSharedPreferences("userData", MODE_PRIVATE);
        final String userId = preferences.getString("id", "");
        backIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        backLeftIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mDatabase.child("request").child("accepted").orderByChild("userID").equalTo(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                historyBeens.clear();
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    @SuppressWarnings("unchecked")
                    Map<String, Object> map = (Map<String, Object>) ds.getValue();
                    String username = map.get("driver_name").toString();
                    String date = map.get("datetime").toString();
                    String user_phone = map.get("driver_phone").toString();
                    String user_photo = map.get("driver_photo").toString();
                    int status = Integer.valueOf(map.get("status").toString());

                    if (status == 1) {
                        historyBeens.add(new historyBean(username, user_phone, date, user_photo));
                    }
                }
                adapter = new historyAdapter(History.this, historyBeens);
                lv.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


}
