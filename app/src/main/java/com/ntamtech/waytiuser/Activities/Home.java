package com.ntamtech.waytiuser.Activities;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ntamtech.waytiuser.Fragments.MapFragment;
import com.ntamtech.waytiuser.R;
import java.util.ArrayList;
import java.util.Locale;

import Adapter.CustomDrawerAdapter;
import Bean.DrawerItem;
import Bean.Messages;
import Config.LANG;
import Config.SaveSharedPreference;
import Util.Constraints;

public class Home extends AppCompatActivity {
    private static final int FILTERREQUESTCODE = 106;
    private static final int REQUESTCODEUPDATEMYLOCATION = 107;
    DrawerLayout drawer;
    ListView drawerList;
    ListView drawerListLeft;
    ImageView menuIV;
    ImageView notificationIV;
    ImageView menuIVAr;
    ImageView notificationIVAr;
    FragmentManager manager;
    NavigationView navigationViewLeft;
    NavigationView navigationViewRight;
    DatabaseReference mDatabase;
    TextView notifyLeftTV;
    TextView notifyRightTV;
    FrameLayout frameLayout;
    FrameLayout frameLayoutMain;
    ImageView ivFilter;
    MapFragment mapFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

         FirebaseAuth mAuth;

        SharedPreferences preferences = getSharedPreferences("language", MODE_PRIVATE);
        String lang = preferences.getString("lang", "");
        if (lang.equals("ar")){
            SetLanguage(getApplicationContext(), LANG.ARABIC);
        }
        setContentView(R.layout.activity_home);
        init();

        menuIV.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                drawer.openDrawer(Gravity.END);
                return false;
            }
        });


        SharedPreferences sharedPreferences = getSharedPreferences("userData", MODE_PRIVATE);
        final String phone = sharedPreferences.getString("phone", "");
        final String profileImg = sharedPreferences.getString("profileImg", "");
        final String name = sharedPreferences.getString("name", "");
        final String city = sharedPreferences.getString("city", "");

        if (lang.equals("ar")) {

            View headerRight = navigationViewRight.getHeaderView(0);
            ImageView profileIVR = (ImageView) headerRight.findViewById(R.id.profile_image);
            TextView nameTVR = (TextView) headerRight.findViewById(R.id.name);
            TextView phoneTVR = (TextView) headerRight.findViewById(R.id.phone);
            TextView tvCity = (TextView) headerRight.findViewById(R.id.city);

            Glide.with(getApplicationContext()).load(profileImg).into(profileIVR);
            nameTVR.setText(phone+"");
            phoneTVR.setText(name);
            tvCity.setText(city);
            SetLanguage(getApplicationContext(), LANG.ARABIC);
            menuIVAr.setVisibility(View.GONE);
            notificationIVAr.setVisibility(View.GONE);
            notifyRightTV.setVisibility(View.GONE);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(R.id.nav_left_view));
            menuIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.openDrawer(Gravity.END);
                }
            });
        } else {
            View headerLeft = navigationViewLeft.getHeaderView(0);
            ImageView profileIV = (ImageView) headerLeft.findViewById(R.id.profile_image);
            TextView nameTV = (TextView) headerLeft.findViewById(R.id.name);
            TextView phoneTV = (TextView) headerLeft.findViewById(R.id.phone);
            TextView tvCity = (TextView) headerLeft.findViewById(R.id.city);


            Glide.with(getApplicationContext()).load(profileImg).into(profileIV);
            nameTV.setText(phone+"");
            phoneTV.setText(name);
            tvCity.setText(city);
            SetLanguage(getApplicationContext(), LANG.ENGLISH);
            menuIV.setVisibility(View.GONE);
            notificationIV.setVisibility(View.GONE);
            notifyLeftTV.setVisibility(View.GONE);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(R.id.nav_right_view));

            menuIVAr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.openDrawer(Gravity.START);
                }
            });


        }

        final ArrayList<DrawerItem> dataList = new ArrayList<DrawerItem>();
        //dataList.add(new DrawerItem(getResources().getString(R.string.edit_location), R.drawable.editlocation));
        dataList.add(new DrawerItem(getResources().getString(R.string.map), R.drawable.map));
        dataList.add(new DrawerItem(getResources().getString(R.string.conversations), R.drawable.conversations));
        dataList.add(new DrawerItem(getResources().getString(R.string.history), R.drawable.history));
        dataList.add(new DrawerItem(getResources().getString(R.string.complaints), R.drawable.complaints));
        dataList.add(new DrawerItem(getResources().getString(R.string.aboutUs), R.drawable.about_us));
        dataList.add(new DrawerItem(getResources().getString(R.string.contactUs), R.drawable.contact_us));
        dataList.add(new DrawerItem(getResources().getString(R.string.exit), R.drawable.logout));

        drawerList.setAdapter(new CustomDrawerAdapter(Home.this, R.layout.custom_drawer_item, dataList));
        drawerListLeft.setAdapter(new CustomDrawerAdapter(Home.this, R.layout.custom_drawer_item_left, dataList));


        drawerList.setAdapter(new CustomDrawerAdapter(Home.this, R.layout.custom_drawer_item,dataList));
        drawerListLeft.setAdapter(new CustomDrawerAdapter(Home.this, R.layout.custom_drawer_item_left,dataList));

        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                navigation(position);
                drawer.closeDrawer(GravityCompat.END);
            }
        });
        drawerListLeft.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                navigation(position);
                drawer.closeDrawer(GravityCompat.START);
            }
        });

        mapFragment = new MapFragment();
        manager = getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.mainLayout, mapFragment).commit();

//        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//        fragmentTransaction.add(R.id.mainLayout, new MapFragment(), "fragA");
//        fragmentTransaction.add(R.id.mainLayout, new FragmentUser(), "fragB");
//        fragmentTransaction.commit();

        //switchToA();

        notificationIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent mainIntent = new Intent(Home.this, Chat.class);
                startActivity(mainIntent);
            }
        });
        notificationIVAr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent mainIntent = new Intent(Home.this, Chat.class);
                startActivity(mainIntent);
            }
        });

        menuIV.bringToFront();
    }


    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (drawer.isDrawerOpen(GravityCompat.END)) {  /*Closes the Appropriate Drawer*/
            drawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }
    }

    public void navigation(int position) {
        //position = position - 1;
        /*if  (position == -1){
            final Intent mainIntent = new Intent(Home.this, SetLocation.class);
            startActivityForResult(mainIntent,REQUESTCODEUPDATEMYLOCATION);
        }else*/ if (position == 0) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (position == 1) {
            final Intent mainIntent = new Intent(Home.this, Chat.class);
            startActivity(mainIntent);
        } else if (position == 2) {

            final Intent mainIntent = new Intent(Home.this, History.class);
            startActivity(mainIntent);
        } else if (position == 3) {

            final Intent mainIntent = new Intent(Home.this, Complains.class);
            startActivity(mainIntent);
        }else if (position == 5) {

            final Intent mainIntent = new Intent(Home.this, ContactUs.class);
            startActivity(mainIntent);
        }
//
        else if (position == 4) {

            final Intent mainIntent = new Intent(Home.this, AboutUs.class);
            startActivity(mainIntent);
        }
//
        else if (position == 6) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Home.this);
            alertDialogBuilder
                    .setMessage(getResources().getString(R.string.logout))
                    .setPositiveButton(getResources().getString(R.string.exit), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //auth.signOut();
                            SaveSharedPreference.clearUserName(getApplicationContext());
                            final Intent mainIntent = new Intent(Home.this, Login.class);
                            startActivity(mainIntent);
                            finish();


                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }

//==========================

    public static void SetLanguage(Context ctx, LANG selectedLang) {
        if (selectedLang == LANG.ENGLISH) {
            // Change layout to en
            Locale mLocale = new Locale("en");
            Locale.setDefault(mLocale);
            Configuration config = ctx.getResources().getConfiguration();

            if (!config.locale.equals(mLocale)) {
                config.locale = mLocale;
                ctx.getResources().updateConfiguration(config, null);
            }
//            SharedPrefCustom.saveLocalization(ctx, english);
        } else {
            // Change layout to ar
            Locale mLocale = new Locale("ar");
            Locale.setDefault(mLocale);
            Configuration config = ctx.getResources().getConfiguration();
            if (!config.locale.equals(mLocale)) {
                config.locale = mLocale;
                ctx.getResources().updateConfiguration(config, null);
            }
//            SharedPrefCustom.saveLocalization(ctx, arabic);
        }
    }
//==========================================================================

    public void init() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerList = (ListView) findViewById(R.id.adapter);
        drawerListLeft = (ListView) findViewById(R.id.adapterLeft);
        menuIV = (ImageView) findViewById(R.id.menuEn);
        notificationIV = (ImageView) findViewById(R.id.notification);
        menuIVAr = (ImageView) findViewById(R.id.menuAr);
        notificationIVAr = (ImageView) findViewById(R.id.notificationAr);
        frameLayout = (FrameLayout) findViewById(R.id.mainLayout);
        frameLayoutMain = (FrameLayout) findViewById(R.id.main);
        navigationViewLeft = (NavigationView) findViewById(R.id.nav_left_view);
        navigationViewRight = (NavigationView) findViewById(R.id.nav_right_view);
        notifyLeftTV = (TextView) findViewById(R.id.notifyLeftTest);
        notifyRightTV = (TextView) findViewById(R.id.notifyRight);
        ivFilter = (ImageView)findViewById(R.id.filter);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        menuIV.bringToFront();
        notificationIV.bringToFront();
        menuIVAr.bringToFront();
        notificationIVAr.bringToFront();
        notifyLeftTV.bringToFront();


        menuIV.bringToFront();
        notifyRightTV.bringToFront();
        ivFilter.bringToFront();
        notification();
  //      acceptedLisner();
//        notificationIV.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                final Intent mainIntent = new Intent(Home.this, Chat.class);
//                startActivity(mainIntent);
//                return false;
//            }
//        });
//        notificationIVAr.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                final Intent mainIntent = new Intent(Home.this, Chat.class);
//                startActivity(mainIntent);
//                return false;
//            }
//        });

        ivFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(Home.this,FilterActivity.class),FILTERREQUESTCODE);
            }
        });
    }

    public void notification() {
        SharedPreferences preferences = getSharedPreferences("userData", MODE_PRIVATE);
        final String userId = preferences.getString("id", "");
        mDatabase.child("chat").orderByChild("receiver_id").equalTo(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int size = 0;
                if (dataSnapshot.exists()){
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        if (ds.getValue(Messages.class).getUserSeen().equals("0")) {
                            size++;
                        }
                    }
                }
                notifyLeftTV.setText(size + "");
                notifyRightTV.setText(size + "");
            }
            @Override
            public void onCancelled(DatabaseError databaseError){
            }
        });

    }



    //=============================================

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences1 = getSharedPreferences("userData", MODE_PRIVATE);
        final String userId = sharedPreferences1.getString("id", "");
        mDatabase.child("user").child(userId).child("block").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue()!=null && (Boolean) dataSnapshot.getValue() == true){
                    AlertDialog alertDialog = new AlertDialog.Builder(Home.this).create();
                    alertDialog.setMessage(getString(R.string.blok_msg) );
                    alertDialog.setCancelable(false);
                    alertDialog.show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && requestCode == FILTERREQUESTCODE){
            int capacity = data.getIntExtra(Constraints.INTENT_KEY_CAPACITY,0);
            String price = data.getStringExtra(Constraints.INTENT_KEY_PRICE);
            mapFragment.updateMap(capacity,price);
        } else if(resultCode == RESULT_OK && requestCode == REQUESTCODEUPDATEMYLOCATION){
            mapFragment.updateMyLocation();
        }
    }
}
