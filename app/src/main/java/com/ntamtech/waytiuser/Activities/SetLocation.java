package com.ntamtech.waytiuser.Activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ntamtech.waytiuser.R;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class SetLocation extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnCameraMoveListener {
    GoogleMap map;
    Marker marker;
    ImageView locationIV;
    Geocoder geocoder;
    List<Address> addresses;
    private LocationManager locationManager;
    private LocationListener listener;
    int flag = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_location);
        geocoder = new Geocoder(this, Locale.getDefault());
        locationIV = (ImageView) findViewById(R.id.back);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map1);
        mapFragment.getMapAsync(this);
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        }

        listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                LatLng pp = new LatLng(location.getLatitude(), location.getLongitude());
                if (flag == 0) {
                    map.clear();
                    if (marker == null){
                        marker = map.addMarker(new MarkerOptions().position(pp)
                                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.address)));
                    }
                    map.moveCamera(CameraUpdateFactory.newLatLng(pp));
                    flag = 1;
                }
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {
                buildAlertMessageNoGps();
            }
        };

        locationIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    addresses = geocoder.getFromLocation(map.getCameraPosition().target.latitude, map.getCameraPosition().target.longitude, 1);
                    // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    String knownName = addresses.get(0).getFeatureName();
                    SharedPreferences.Editor editor = getSharedPreferences("userData", MODE_PRIVATE).edit();
                    if (address == null) {
                        address = "";
                    }
                    if (city == null) {
                        city = "";
                    }
                    if (state == null) {
                        state = "";
                    }
                    if (country == null) {
                        country = "";
                    }

                    editor.putString("address", address + " " + city + " " + state + " " + country);
                    editor.putString("lng", map.getCameraPosition().target.longitude + "");
                    editor.putString("lat", map.getCameraPosition().target.latitude + "");
                    editor.commit();
                    setResult(SetLocation.this.RESULT_OK);
                    finish();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0, listener);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 0, listener);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setOnCameraMoveListener(this);

    }

    @Override
    public void onCameraMove() {
        if (marker == null){
            marker = map.addMarker(new MarkerOptions().position(map.getCameraPosition().target)
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.user_location)));
        }else {
            marker.setPosition(map.getCameraPosition().target);
        }

    }

    private void buildAlertMessageNoGps() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        Toast.makeText(getApplicationContext(),"cancel gps", Toast.LENGTH_SHORT).show();
                    }
                });
        // final AlertDialog alert = builder.create();
        builder.show();
    }
}
