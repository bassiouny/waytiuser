package com.ntamtech.waytiuser.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ntamtech.waytiuser.R;

import java.util.ArrayList;
import java.util.Map;

import Adapter.chatAdapter;
import Bean.chatBean;

public class Chat extends AppCompatActivity {

    chatAdapter adapter;
    ListView lv;
    ImageView backLeftIV;
    ArrayList<chatBean> chatBeen = new ArrayList<>();
    private DatabaseReference mDatabase;
    ImageView backIV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        lv = (ListView) findViewById(R.id.lv);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        backIV = (ImageView) findViewById(R.id.back);
        backLeftIV = (ImageView)findViewById(R.id.backLeft);
        SharedPreferences sharedPreferences = getSharedPreferences("language", MODE_PRIVATE);
        String lang = sharedPreferences.getString("lang", "");
        if (lang.equals("ar")){
            backLeftIV.setVisibility(View.GONE);
        }else {
            backIV.setVisibility(View.GONE);
        }
        SharedPreferences preferences = getSharedPreferences("userData", MODE_PRIVATE);
        final String userId = preferences.getString("id", "");

        backIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        backLeftIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        mDatabase.child("chat").orderByChild("receiver_id").equalTo(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                chatBeen.clear();
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    @SuppressWarnings("unchecked")
                    Map<String, Object> map = (Map<String, Object>) ds.getValue();
                    String sender_photo = map.get("sender_photo").toString();
                    String sender_name = map.get("sender_name").toString();
                    String sender_id = map.get("sender_id").toString();
                    String msg = map.get("msg").toString();
                    String seen = map.get("userSeen").toString();
                    Boolean userDelete = (Boolean) map.get("userDelete");

                    if (userDelete == null){
                        chatBeen.add(new chatBean(sender_id, sender_photo, sender_name, msg, seen));

                    }else {
                        if (userDelete == false){
                            chatBeen.add(new chatBean(sender_id, sender_photo, sender_name, msg, seen));
                        }
                    }


                }
                adapter = new chatAdapter(Chat.this, chatBeen);
                lv.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Intent mainIntent = new Intent(Chat.this, SingleMsg.class);
                startActivity(mainIntent);

                SharedPreferences.Editor editor = getSharedPreferences("chatData", MODE_PRIVATE).edit();
                editor.putString("userId", chatBeen.get(position).getReceiverID());
                editor.putString("name", chatBeen.get(position).getName());
                editor.putString("img", chatBeen.get(position).getImg());
                editor.commit();

            }
        });
    }
}
