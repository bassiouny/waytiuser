package com.ntamtech.waytiuser.Activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ntamtech.waytiuser.R;

public class AboutUs extends AppCompatActivity {

    ImageView backIV;
    ImageView backLeftIV;
    TextView aboutTV;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        intial();
        events();

        SharedPreferences preferences = getSharedPreferences("language", MODE_PRIVATE);
        String lang = preferences.getString("lang", "");
        if (lang.equals("ar")){
            backLeftIV.setVisibility(View.GONE);
        }else {
            backIV.setVisibility(View.GONE);
        }

    }

    public void intial(){
        backIV = (ImageView)findViewById(R.id.back);
        backLeftIV = (ImageView)findViewById(R.id.backLeft);
        aboutTV = (TextView)findViewById(R.id.about);
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }

    public void events(){
        backIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        backLeftIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mDatabase.child("setting").child("about").child("body").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                aboutTV.setText(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

}
