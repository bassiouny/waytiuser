package com.ntamtech.waytiuser.Activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.ntamtech.waytiuser.R;

import java.net.PasswordAuthentication;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import Config.RestInterface;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

public class ForgetPassword extends AppCompatActivity {

    EditText phoneET;
    EditText passwordET;
    EditText codeET;
    Button submitPassword;
    Button submitCode;
    Button submitPhone;
    int code;
    String phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        init();
        events();

    }

    public void init() {
        phoneET = (EditText) findViewById(R.id.phone);
        passwordET = (EditText) findViewById(R.id.password);
        codeET = (EditText) findViewById(R.id.code);
        submitCode = (Button) findViewById(R.id.submitCode);
        submitPassword = (Button) findViewById(R.id.submitPassword);
        submitPhone = (Button) findViewById(R.id.submitPhone);

        passwordET.setVisibility(View.GONE);
        codeET.setVisibility(View.GONE);
        submitCode.setVisibility(View.GONE);
        submitPassword.setVisibility(View.GONE);
    }

    public void events() {

        //
        // phone

        submitPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //   getCode();
                //  phone = phoneET.getText().toString();

//                final FirebaseAuth auth = FirebaseAuth.getInstance();
//                String emailAddress = "ayaomar003@gmail.com";

//                auth.confirmPasswordReset(String code , )

//                auth.sendPasswordResetEmail(emailAddress).addOnSuccessListener(new OnSuccessListener<Void>() {
//                    @Override
//                    public void onSuccess(Void aVoid) {


//                    }
//                });


            }
        });


        submitCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int codeT = Integer.valueOf(codeET.getText().toString());
                if (codeT == code) {

                    codeET.setVisibility(View.GONE);
                    codeET.setVisibility(View.GONE);
                    passwordET.setVisibility(View.VISIBLE);
                    submitPassword.setVisibility(View.VISIBLE);
                }
            }
        });
        submitPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetPassword();
            }
        });
    }

    public void getCode() {

        phone = phoneET.getText().toString();
        Random rand = new Random();
        code = rand.nextInt(9999);
        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.baseURL).build();
        RestInterface restInterface = adapter.create(RestInterface.class);
        restInterface.editPassword(phone, code, new Callback<String>() {

            @Override
            public void success(String model, Response response) {

                phoneET.setVisibility(View.GONE);
                submitPhone.setVisibility(View.GONE);
                codeET.setVisibility(View.VISIBLE);
                submitCode.setVisibility(View.VISIBLE);

            }

            @Override
            public void failure(RetrofitError error) {

                if (error.getKind().equals(RetrofitError.Kind.NETWORK)) {
                    Toast.makeText(getApplicationContext(), "Please Check your Connection!", Toast.LENGTH_LONG).show();
                }

            }
        });

    }

    public void resetPassword() {

        final String pass = passwordET.getText().toString();

        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        String email = "01148958554" + "@gmail.com";
        AuthCredential credential = EmailAuthProvider.getCredential(email, pass);
        user.reauthenticate(credential)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            user.updatePassword(pass).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {

                                        Toast.makeText(getApplicationContext(), "Password updated!", Toast.LENGTH_LONG).show();

                                    } else {
                                        Toast.makeText(getApplicationContext(), "Error password not updated!", Toast.LENGTH_LONG).show();

                                    }
                                }
                            });
                        } else {

                            Toast.makeText(getApplicationContext(), task.getResult().toString(), Toast.LENGTH_LONG).show();

                        }
                    }
                });

//        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
//        String newPassword = pass;
//
//        user.updatePassword(newPassword)
//                .addOnCompleteListener(new OnCompleteListener<Void>() {
//                    @Override
//                    public void onComplete(@NonNull Task<Void> task) {
//                        if (task.isSuccessful()) {
//                            Log.d(TAG, "User password updated.");
//                        }
//                    }
//                });
    }
}
