package com.ntamtech.waytiuser.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ntamtech.waytiuser.R;

import Bean.RequestBean;

import static com.twilio.client.impl.TwilioImpl.context;

public class PendingRequest extends AppCompatActivity {
    private DatabaseReference mDatabase;

    Button cancelRequest;
    int flag = 0;
    ImageView carImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_request);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        cancelRequest = (Button) findViewById(R.id.cancel);
        carImg = (ImageView)findViewById(R.id.carImg);

        SharedPreferences preferences = getSharedPreferences("key", MODE_PRIVATE);
        final String key = preferences.getString("key", "");


        cancelRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDatabase.child("request").child("pending").child(key).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        finish();
                    }
                });
            }
        });

        acceptedLisner();
        failedListener();
    }

    public void acceptedLisner() {
        SharedPreferences sharedPreferences = getSharedPreferences("userData", MODE_PRIVATE);
        final String userID = sharedPreferences.getString("id", "");
        SharedPreferences sp = getSharedPreferences("chatData", MODE_PRIVATE);
        final String driverID = sp.getString("userId", "");
        final String isDrinkable = sp.getString("isDrinkable", "");

        if (isDrinkable.equals("false")){
            carImg.setImageResource(R.drawable.yellowcar);
        }

        mDatabase.child("request").child("accepted").orderByChild("userID").equalTo(userID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    String driverIDS = ds.getValue(RequestBean.class).getDriverID();
                    int status = ds.getValue(RequestBean.class).getStatus();
                    if (driverIDS.equals(driverID) && status == 0) {
                        if (flag != 0) {
                            PendingRequest.this.finish();
                            SharedPreferences.Editor editor = getSharedPreferences("keyData", MODE_PRIVATE).edit();
                            editor.putString("key", ds.getKey());
                            editor.commit();


                        }
                    }
                }
                flag = 1;
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public  void failedListener(){
        SharedPreferences sharedPreferences = getSharedPreferences("userData", MODE_PRIVATE);
        final String userID = sharedPreferences.getString("id", "");
        SharedPreferences sp = getSharedPreferences("chatData", MODE_PRIVATE);
        final String driverID = sp.getString("userId", "");
        mDatabase.child("request").child("failed").orderByChild("userID").equalTo(userID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    int isRejected = ds.getValue(RequestBean.class).getIsRejected();
                    String driver = ds.getValue(RequestBean.class).getDriverID();

                    if (isRejected == 1 && flag!=0 && flag!=1 && driver.equals(driverID)){
                        mDatabase.child("request").child("failed").child(ds.getKey()).child("isRejected").setValue(0);

                        if(((Activity) context) !=null)
                        {
                            alertMsg(getString(R.string.refused));
                        }else {
                            finish();
                        }
                    }
                }
                flag = 2;
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    //================================

    public void alertMsg(String msg){
        final AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            builder = new AlertDialog.Builder(PendingRequest.this, android.R.style.Theme_DeviceDefault_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(PendingRequest.this);
        }
        builder.setTitle(msg)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        PendingRequest.this.finish();
                        dialog.dismiss();
                    }
                })

                .setCancelable(false)
                .show();
    }
}
