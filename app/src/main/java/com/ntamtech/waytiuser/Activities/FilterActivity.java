package com.ntamtech.waytiuser.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.ntamtech.waytiuser.R;

import java.io.File;

import Util.Constraints;

public class FilterActivity extends AppCompatActivity {

    EditText etPrice;
    Spinner spCapacity;
    Button btnOK;
    CheckBox checkBoxSearchByPrice,checkBoxSearchByCapacity;
    int capacitySelected=0;
    String []capacity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        this.setFinishOnTouchOutside(false);
        initView();
        initData();
        initEvents();
    }



    private void initView() {
        etPrice = (EditText) findViewById(R.id.et_price);
        spCapacity = (Spinner) findViewById(R.id.sp_capacity);
        btnOK = (Button) findViewById(R.id.btn_ok);
        checkBoxSearchByPrice = (CheckBox) findViewById(R.id.ch_search_by_price);
        checkBoxSearchByCapacity = (CheckBox) findViewById(R.id.ch_search_by_capacity);
    }
    private void initEvents() {
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(spCapacity.getSelectedItemPosition()==0)
                    capacitySelected = 0;
                else {
                    try {
                        capacitySelected = Integer.parseInt(spCapacity.getSelectedItem().toString());
                    }catch (Exception e){
                        e.printStackTrace();
                        capacitySelected=0;
                    }
                }
                if(checkBoxSearchByPrice.isChecked() && etPrice.getText().toString().trim().equals("0")) {
                    Toast.makeText(FilterActivity.this, R.string.valid_price, Toast.LENGTH_SHORT).show();
                }
                Intent resultIntent = new Intent();
                resultIntent.putExtra(Constraints.INTENT_KEY_CAPACITY,capacitySelected);
                resultIntent.putExtra(Constraints.INTENT_KEY_PRICE, etPrice.getText().toString());
                setResult(FilterActivity.this.RESULT_OK, resultIntent);
                finish();
            }
        });
        checkBoxSearchByPrice.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    etPrice.setEnabled(true);
                }else {
                    etPrice.setEnabled(false);
                    etPrice.setText("0");
                }
            }
        });
        checkBoxSearchByCapacity.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
    }

    private void initData() {
        capacity = new String[] {"الكل","12","18","19","28","30","32"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, capacity);
        spCapacity.setAdapter(adapter);
    }

/*
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }*/
}
