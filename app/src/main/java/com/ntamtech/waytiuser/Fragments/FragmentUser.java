package com.ntamtech.waytiuser.Fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ntamtech.waytiuser.Activities.SingleMsg;
import com.ntamtech.waytiuser.LatLngInterpolator;
import com.ntamtech.waytiuser.MarkerAnimation;
import com.ntamtech.waytiuser.R;

import Bean.DriverBean;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Ayaom on 7/17/2017.
 */

public class FragmentUser extends Fragment implements OnMapReadyCallback {

    GoogleMap map;
    Marker marker;
    Marker driverMarker;
    private DatabaseReference mDatabase;
    TextView nameTV;
    TextView phoneTV;
    ImageView profile_image;
    RelativeLayout arrived;
    RelativeLayout notArrived;
    int totalNoOfRate;
    int totalRate;
    float rate;
    Button chat;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_user, container, false);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        nameTV = (TextView) v.findViewById(R.id.name);
        phoneTV = (TextView) v.findViewById(R.id.phone);
        profile_image = (ImageView) v.findViewById(R.id.profile_image);
        chat = (Button) v.findViewById(R.id.chat);
        notArrived = (RelativeLayout) v.findViewById(R.id.notArrived);
        arrived = (RelativeLayout) v.findViewById(R.id.arrived);

        SharedPreferences preferences = getActivity().getSharedPreferences("chatData", MODE_PRIVATE);
        final String name = preferences.getString("name", "");
        final String phone = preferences.getString("phone", "");
        final String img = preferences.getString("img", "");

        notArrived.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater factory = LayoutInflater.from(getActivity());
                final View notArrivedDialogView = factory.inflate(R.layout.not_arrived, null);
                final android.support.v7.app.AlertDialog carDialog = new android.support.v7.app.AlertDialog.Builder(getActivity()).create();
                carDialog.setView(notArrivedDialogView);
                carDialog.setCancelable(true);
                carDialog.show();
            }
        });

        arrived.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sp = getActivity().getSharedPreferences("chatData", MODE_PRIVATE);
                final String driverID = sp.getString("userId", "");
                LayoutInflater factory = LayoutInflater.from(getActivity());
                final View arrivedDialogView = factory.inflate(R.layout.arrived, null);
                TextView nameTV = (TextView) arrivedDialogView.findViewById(R.id.name);
                Button accept = (Button) arrivedDialogView.findViewById(R.id.accept);
                ImageView driverImg = (ImageView) arrivedDialogView.findViewById(R.id.driverImg);
                RatingBar ratingBar = (RatingBar) arrivedDialogView.findViewById(R.id.rating);
                nameTV.setText(name);

                Glide.with(getContext()).load(img).into(driverImg);

                final android.support.v7.app.AlertDialog carDialog = new android.support.v7.app.AlertDialog.Builder(getActivity()).create();
                carDialog.setView(arrivedDialogView);
                carDialog.setCancelable(true);
                carDialog.show();

                accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        SharedPreferences sp = getActivity().getSharedPreferences("keyData", MODE_PRIVATE);
                        final String key = sp.getString("key", "");

                        mDatabase.child("request").child("accepted").child(key).child("status").setValue(1).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                int totalNumRate = Integer.valueOf(totalNoOfRate) + 1;
                                int sumOfRate = (int) (Integer.valueOf(totalRate) + rate);
                                double totalRate = sumOfRate / totalNumRate;
                                mDatabase.child("driver").child(driverID).child("total_no_of_rate").setValue(totalNumRate);
                                mDatabase.child("driver").child(driverID).child("total_rate").setValue(totalRate);
                                carDialog.dismiss();
                                SharedPreferences.Editor editor = getContext().getSharedPreferences("arrived", MODE_PRIVATE).edit();
                                editor.putString("arrived", "arrived");
                                editor.commit();
                            }
                        });
                    }
                });

                ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                    @Override
                    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                        rate = rating;
                    }
                });
            }
        });

        chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SingleMsg.class));
            }
        });


        nameTV.setText(name);
        phoneTV.setText(phone);

        Glide.with(getContext()).load(img).into(profile_image);
        profile_image.bringToFront();
        return v;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        SharedPreferences preferences = getActivity().getSharedPreferences("userData", MODE_PRIVATE);
        final String lat = preferences.getString("lat", "");
        final String lng = preferences.getString("lng", "");


        LatLng pp = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));

        marker = map.addMarker(new MarkerOptions().position(pp)
                .icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.user_location))));
        map.moveCamera(CameraUpdateFactory.newLatLng(pp));
        SharedPreferences sp = getActivity().getSharedPreferences("chatData", MODE_PRIVATE);
        final String driverID = sp.getString("userId", "");

        mDatabase.child("driver").child(driverID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                double lat = dataSnapshot.getValue(DriverBean.class).getLocation_latitude();
                double lng = dataSnapshot.getValue(DriverBean.class).getLocation_longitude();

                totalNoOfRate = dataSnapshot.getValue(DriverBean.class).getTotal_no_of_rate();
                totalRate = dataSnapshot.getValue(DriverBean.class).getTotal_rate();

                if (driverMarker == null) {

                    driverMarker = map.addMarker(new MarkerOptions().position(new LatLng(lat, lng))
                            .icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.green_car))));

                } else {

                    MarkerAnimation.animateMarkerToICS(driverMarker, new LatLng(lat, lng), new LatLngInterpolator.Spherical());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapUser);
        mapFragment.getMapAsync(this);
    }



}
