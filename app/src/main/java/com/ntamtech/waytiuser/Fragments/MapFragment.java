package com.ntamtech.waytiuser.Fragments;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ntamtech.waytiuser.Activities.ForgetPasswordActivity;
import com.ntamtech.waytiuser.Activities.PendingRequest;
import com.ntamtech.waytiuser.Activities.SingleMsg;
import com.ntamtech.waytiuser.LatLngInterpolator;
import com.ntamtech.waytiuser.MarkerAnimation;
import com.ntamtech.waytiuser.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import Bean.DriverBean;
import Bean.RequestBean;
import Config.LANG;
import Config.RestInterface;
import Util.DirectionsJSONParser;
import Util.Utility;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.content.Context.LOCATION_SERVICE;
import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends Fragment implements OnMapReadyCallback {

    private static final String MYMARKER = "my marker";
    GoogleMap map;
    private DatabaseReference mDatabase;
    Map<String, Object> drinkable;
    Map<String, Object> unDrinkable;
    RelativeLayout drinkableR;
    RelativeLayout unDrinkableR;
    RelativeLayout relativeLayout;
    LinearLayout linearLayout;
    ImageView profile_image;
    TextView nameTV;
    TextView phoneTV;
    RelativeLayout arrived;
    RelativeLayout notArrived;
    int totalNoOfRate;
    int totalRate;
    float rate;
    Button chat;
    int flag = 0;
    Marker marker;
    Marker myMarker;
    Marker currentDriver;
    MarkerOptions myPlace;

    Button myLocation;
    Button myLocationUser;

    int markerIcon;
    private int capacity;
    private double price;
    ImageView ivSetNewPlace,ivCancleNewPlace;

    public MapFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_map, container, false);

        try
        {
            init(v);
            hideViews();
            events();
        }catch (Exception  e){
            Toast.makeText(getContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map1);
        mapFragment.getMapAsync(this);

        SharedPreferences preferences = getContext().getSharedPreferences("language", MODE_PRIVATE);
        String lang = preferences.getString("lang", "");
        if (lang.equals("ar")) {
            SetLanguage(getContext(), LANG.ARABIC);
        } else {
            SetLanguage(getContext(), LANG.ENGLISH);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        acceptedLisner();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        SharedPreferences preferences = getActivity().getSharedPreferences("userData", MODE_PRIVATE);
        final String lat = preferences.getString("lat", "");
        final String lng = preferences.getString("lng", "");
        final String userID = preferences.getString("id", "");
        final String userPhone = preferences.getString("phone", "");
        final String userPhoto = preferences.getString("profileImg", "");
        final String username = preferences.getString("name", "");

        getAllCars(false);
        acceptedLisner();
        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                String carData = marker.getSnippet();
                if(carData.equals(MYMARKER)){
                    if(map==null)
                        return false;
                    map.clear();
                    getMyLocation();
                    ivCancleNewPlace.setVisibility(View.VISIBLE);
                    ivSetNewPlace.setVisibility(View.VISIBLE);
                    myLocationUser.setVisibility(View.GONE);
                    map.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
                        @Override
                        public void onCameraMove() {
                            myMarker.setPosition(map.getCameraPosition().target);
                        }
                    });
                } else if (Utility.isNotNull(carData)) {

                    Log.e( "onMarkerClick: ",carData.toString() );
                    String[] parts = carData.split(",");
                    final String driverID = parts[0];
                    String photo = parts[1];
                    String mobile = parts[2];
                    String weight = parts[3];
                    String rate = parts[4];
                    String rateNum = parts[5];
                    String driverPhoto = parts[6];
                    String isDrinkable = parts[7];
                    String price = parts[8];
                    final String token = parts[9];
                    double totalRate = 0;
                    if (Integer.valueOf(rateNum) != 0) {
                        totalRate = Integer.valueOf(rate) / Integer.valueOf(rateNum);
                    }

                    final View carDialogView;
                    String name = marker.getTitle();
                    LayoutInflater factory = LayoutInflater.from(getActivity());
                    SharedPreferences preferences = getActivity().getSharedPreferences("language", MODE_PRIVATE);
                    String lang = preferences.getString("lang", "");
                    if (lang.equals("ar")) {
                        carDialogView = factory.inflate(R.layout.car_information_dialog, null);
                    } else {
                        carDialogView = factory.inflate(R.layout.car_information_dialog_left, null);
                    }

                    TextView nameTV = (TextView) carDialogView.findViewById(R.id.name);
                    TextView phoneTV = (TextView) carDialogView.findViewById(R.id.phone);
                    TextView weightTV = (TextView) carDialogView.findViewById(R.id.weight);
                    ImageView carPhoto = (ImageView) carDialogView.findViewById(R.id.carImg);
                    ImageView closeIV = (ImageView) carDialogView.findViewById(R.id.close);
                    Button sendRequest = (Button) carDialogView.findViewById(R.id.sendRequest);
                    RatingBar ratingBar = (RatingBar) carDialogView.findViewById(R.id.rating);
                    TextView tvprice = (TextView) carDialogView.findViewById(R.id.tv_price);
                    ratingBar.setRating((int) totalRate);

                    Button chat = (Button) carDialogView.findViewById(R.id.chat);
                    nameTV.setText(name);
                    phoneTV.setText(mobile);
                    weightTV.setText(weight);
                    Glide.with(getContext()).load(photo).into(carPhoto);
                    float totalPrice = 0.0f;
                    if(!price.isEmpty() && !weight.isEmpty())
                        totalPrice = Float.parseFloat(price) * Float.parseFloat(weight);
                    tvprice.setText(String.valueOf(totalPrice));

                    SharedPreferences.Editor editor = getContext().getSharedPreferences("chatData", MODE_PRIVATE).edit();
                    editor.putString("userId", driverID);
                    editor.putString("name", name);
                    editor.putString("phone", mobile);
                    editor.putString("img", photo);
                    editor.putString("driverPhoto", driverPhoto);
                    editor.putString("isDrinkable", isDrinkable);
                    editor.commit();

                    SharedPreferences.Editor e = getContext().getSharedPreferences("key", MODE_PRIVATE).edit();
                    e.putString("key", driverID + "-" + userID);
                    e.commit();
                    final android.support.v7.app.AlertDialog carDialog = new android.support.v7.app.AlertDialog.Builder(getActivity()).create();
                    carDialog.setView(carDialogView);
                    carDialog.setCancelable(true);
                    carDialog.show();
                    marker.hideInfoWindow();


                    closeIV.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            carDialog.dismiss();
                        }
                    });

                    sendRequest.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Map<String, Object> data = new HashMap<String, Object>();
                            data.put("driverID", driverID);
                            data.put("userID", userID);
                            try {
                                data.put("user_latitude", Double.parseDouble(lat));
                                data.put("user_longitude", Double.parseDouble(lng));
                            }catch (Exception e){
                                Toast.makeText(getContext(), "something happened please try again", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            data.put("user_phone", userPhone);
                            data.put("user_photo", userPhoto);
                            data.put("username", username);
                            mDatabase.child("request").child("pending").child(driverID + "-" + userID).setValue(data, new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                    if (databaseError == null) {
                                        startActivity(new Intent(getActivity(), PendingRequest.class));
                                        carDialog.hide();
                                    } else {

                                    }
                                }
                            });
                            new AsyncTask<String, String, String>() {

                                @Override
                                protected String doInBackground(String... params) {
                                    try {
                                        String response = makePostRequest("http://ntam.tech/wayti_notification/send_notification.php",
                                                "title=Wayti" +"&body=لديك طلب توصيل الان" +"&token_id="+token);
                                        return response;
                                    } catch (IOException ex) {
                                        ex.printStackTrace();
                                        return "Sorry Message Not Send";
                                    }
                                }

                                @Override
                                protected void onPostExecute(String s) {
                                    super.onPostExecute(s);
                                }
                            }.execute("");

                        }
                    });

                    chat.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(getActivity(), SingleMsg.class));
                            carDialog.hide();
                        }
                    });
                }

                return true;
            }
        });

        drinkableR.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                drinkableR.setBackground(getResources().getDrawable(R.drawable.rounded_blue));
                unDrinkableR.setBackground(getResources().getDrawable(R.drawable.rounded_border));

                for (String key : unDrinkable.keySet()) {
                    Marker marker = (Marker) unDrinkable.get(key);
                    marker.setVisible(false);
                }

                for (String key : drinkable.keySet()) {

                    Marker marker = (Marker) drinkable.get(key);
                    marker.setVisible(true);
                }
                return false;
            }
        });

        unDrinkableR.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                drinkableR.setBackground(getResources().getDrawable(R.drawable.rounded_border));
                unDrinkableR.setBackground(getResources().getDrawable(R.drawable.rounded_blue));

                for (String key : drinkable.keySet()) {

                    Marker marker = (Marker) drinkable.get(key);
                    marker.setVisible(false);
                }

                for (String key : unDrinkable.keySet()) {
                    Marker marker = (Marker) unDrinkable.get(key);
                    marker.setVisible(true);
                }
                return false;
            }
        });
    }
//=========================================

    public void init(View v) {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        drinkableR = (RelativeLayout) v.findViewById(R.id.drinkable);
        unDrinkableR = (RelativeLayout) v.findViewById(R.id.unDrinkable);
        profile_image = (ImageView) v.findViewById(R.id.profile_image);
        relativeLayout = (RelativeLayout) v.findViewById(R.id.relativeLayout);
        linearLayout = (LinearLayout) v.findViewById(R.id.linearLayout);
        notArrived = (RelativeLayout) v.findViewById(R.id.notArrived);
        arrived = (RelativeLayout) v.findViewById(R.id.arrived);
        chat = (Button) v.findViewById(R.id.chat);
        nameTV = (TextView) v.findViewById(R.id.name);
        phoneTV = (TextView) v.findViewById(R.id.phone);
        myLocation = (Button) v.findViewById(R.id.myLocation);
        myLocationUser = (Button) v.findViewById(R.id.myLocationUser);
        notArrived = (RelativeLayout) v.findViewById(R.id.notArrived);
        arrived = (RelativeLayout) v.findViewById(R.id.arrived);
        profile_image.bringToFront();
        ivSetNewPlace = (ImageView) v.findViewById(R.id.iv_set_new_place);
        ivCancleNewPlace = (ImageView) v.findViewById(R.id.iv_cancle_new_place);

        drinkable = new HashMap<String, Object>();
        unDrinkable = new HashMap<String, Object>();
    }


    //==============

    public void getAllCars(final boolean withFilter) {
        map.clear();
        final View viewMarker = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout, null);
        final TextView tvMarker = (TextView) viewMarker.findViewById(R.id.tv_marker);
        final ImageView ivMarker = (ImageView) viewMarker.findViewById(R.id.img_marker);
        getMyLocation();
        SharedPreferences preferences = getActivity().getSharedPreferences("userData", MODE_PRIVATE);
        final String city = preferences.getString("city", "");

        mDatabase.child("driver").orderByChild("city").equalTo(city).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (withFilter) {
                    int tempcapacity = dataSnapshot.getValue(DriverBean.class).getWeight();
                    double tempprice = (dataSnapshot.getValue(DriverBean.class).getWeight() * dataSnapshot.getValue(DriverBean.class).getPrice());
                    if (capacity != 0 && tempcapacity != capacity)
                        return;
                    if (price != 0.0 && tempprice > price)
                        return;
                }
                if (dataSnapshot.getValue(DriverBean.class).getIsDrinkable() && !dataSnapshot.getValue(DriverBean.class).getExit()) {
                    tvMarker.setText(String.valueOf(dataSnapshot.getValue(DriverBean.class).getPrice()));
                    ivMarker.setImageResource(R.mipmap.green_car);
                    MarkerOptions markerOptions = new MarkerOptions().position(new LatLng(dataSnapshot.getValue(DriverBean.class).getLocation_latitude(), dataSnapshot.getValue(DriverBean.class).getLocation_longitude()))
                            .title(dataSnapshot.getValue(DriverBean.class).getName())
                            .snippet(dataSnapshot.getKey() + "," + dataSnapshot.child("car_photos").getValue(DriverBean.class).getCar_img1() + "," +
                                    dataSnapshot.getValue(DriverBean.class).getMobile() + "," + dataSnapshot.getValue(DriverBean.class).getWeight() + ","
                                    + dataSnapshot.getValue(DriverBean.class).getTotal_rate() + "," + dataSnapshot.getValue(DriverBean.class).getTotal_no_of_rate() + "," + dataSnapshot.getValue(DriverBean.class).getPhoto() + "," + dataSnapshot.getValue(DriverBean.class).getIsDrinkable() + "," + dataSnapshot.getValue(DriverBean.class).getPrice()+","+dataSnapshot.getValue(DriverBean.class).token);
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(getActivity(), viewMarker)));
                    marker = map.addMarker(markerOptions);
                    drinkable.put(dataSnapshot.getKey(), marker);
                } else if (!dataSnapshot.getValue(DriverBean.class).getExit()) {
                    tvMarker.setText(String.valueOf(dataSnapshot.getValue(DriverBean.class).getPrice()));
                    ivMarker.setImageResource(R.mipmap.yellow_car);
                    MarkerOptions markerOptions = new MarkerOptions().position(new LatLng(dataSnapshot.getValue(DriverBean.class).getLocation_latitude(), dataSnapshot.getValue(DriverBean.class).getLocation_longitude()))
                            .title(dataSnapshot.getValue(DriverBean.class).getName())
                            .snippet(dataSnapshot.getKey() + "," + dataSnapshot.child("car_photos").getValue(DriverBean.class).getCar_img1() + "," + dataSnapshot.getValue(DriverBean.class).getMobile() + "," + dataSnapshot.getValue(DriverBean.class).getWeight()
                                    + "," + dataSnapshot.getValue(DriverBean.class).getTotal_rate() + "," + dataSnapshot.getValue(DriverBean.class).getTotal_no_of_rate() + "," + dataSnapshot.getValue(DriverBean.class).getPhoto() + "," + dataSnapshot.getValue(DriverBean.class).getIsDrinkable() + "," + dataSnapshot.getValue(DriverBean.class).getPrice()+ "," +dataSnapshot.getValue(DriverBean.class).token);
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(getActivity(), viewMarker)));
                    marker = map.addMarker(markerOptions);
                    unDrinkable.put(dataSnapshot.getKey(), marker);
                }
                for (String key : unDrinkable.keySet()) {
                    Marker marker = (Marker) unDrinkable.get(key);
                    marker.setVisible(false);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                if (dataSnapshot.getValue(DriverBean.class).getIsDrinkable()) {

                    if (!dataSnapshot.getValue(DriverBean.class).getExit()) {
                        Marker marker = (Marker) drinkable.get(dataSnapshot.getKey());
                        MarkerAnimation.animateMarkerToICS(marker, new LatLng(dataSnapshot.getValue(DriverBean.class).getLocation_latitude(),
                                dataSnapshot.getValue(DriverBean.class).getLocation_longitude()), new LatLngInterpolator.Spherical());
                    } else {
                        Marker marker = (Marker) drinkable.get(dataSnapshot.getKey());
                        if (marker != null) {
                            marker.setVisible(false);
                        }
                    }

                } else {
                    if (!dataSnapshot.getValue(DriverBean.class).getExit()) {
                        Marker marker = (Marker) unDrinkable.get(dataSnapshot.getKey());
                        LatLng latLng = new LatLng(dataSnapshot.getValue(DriverBean.class).getLocation_latitude(), dataSnapshot.getValue(DriverBean.class).getLocation_longitude());
                        if(marker==null ||latLng ==null || new LatLngInterpolator.Spherical()==null){
                            return;
                        }
                        MarkerAnimation.animateMarkerToICS(marker, latLng, new LatLngInterpolator.Spherical());
                    } else {
                        Marker marker = (Marker) unDrinkable.get(dataSnapshot.getKey());
                        if (marker != null) {
                            marker.setVisible(false);

                        }
                    }

                }

                for (String key : unDrinkable.keySet()) {
                    Marker marker = (Marker) unDrinkable.get(key);
                    marker.setVisible(false);
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getActivity(), databaseError+"", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void hideViews() {
        profile_image.setVisibility(View.GONE);
        relativeLayout.setVisibility(View.GONE);
        linearLayout.setVisibility(View.GONE);
        myLocationUser.setVisibility(View.VISIBLE);
        myLocation.setVisibility(View.GONE);

    }

    public void showViews() {
        SharedPreferences preferences = getActivity().getSharedPreferences("chatData", MODE_PRIVATE);
        final String name = preferences.getString("name", "");
        final String phone = preferences.getString("phone", "");
        final String img = preferences.getString("driverPhoto", "");
        nameTV.setText(name);
        phoneTV.setText(phone);
        myLocationUser.setVisibility(View.GONE);
        myLocation.setVisibility(View.VISIBLE);

        Glide.with(getContext()).load(img).into(profile_image);
        profile_image.setVisibility(View.VISIBLE);
        relativeLayout.setVisibility(View.VISIBLE);
        linearLayout.setVisibility(View.VISIBLE);


    }

    //=============================================

    public void acceptedLisner() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("userData", MODE_PRIVATE);
        final String userID = sharedPreferences.getString("id", "");
        mDatabase.child("request").child("accepted").orderByChild("userID").equalTo(userID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (getActivity() != null) {
                    SharedPreferences sp = getActivity().getSharedPreferences("chatData", MODE_PRIVATE);
                    final String driverID = sp.getString("userId", "");

                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        String driverIDS = ds.getValue(RequestBean.class).getDriverID();
                        int status = ds.getValue(RequestBean.class).getStatus();
                        if (driverIDS.equals(driverID) && status == 0) {
                            showViews();
                            getCurrentDriver();
                            break;

                        } else {

                        }
                    }
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getActivity(), databaseError+"", Toast.LENGTH_SHORT).show();
            }
        });
    }

    //=============================

    public void events() {


        myLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if (getActivity() != null) {
                    SharedPreferences preferences = getActivity().getSharedPreferences("userData", MODE_PRIVATE);
                    final String lat = preferences.getString("lat", "");
                    final String lng = preferences.getString("lng", "");
                    LatLng pp = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
                    map.moveCamera(CameraUpdateFactory.newLatLng(pp));
                }*/
                getCurrentlocation();
            }
        });

        myLocationUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if (getActivity() != null) {
                    SharedPreferences preferences = getActivity().getSharedPreferences("userData", MODE_PRIVATE);
                    final String lat = preferences.getString("lat", "");
                    final String lng = preferences.getString("lng", "");
                    LatLng pp = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
                    map.moveCamera(CameraUpdateFactory.newLatLng(pp));
                }*/
                getCurrentlocation();

            }
        });

        notArrived.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater factory = LayoutInflater.from(getActivity());
                final View notArrivedDialogView = factory.inflate(R.layout.not_arrived, null);
                final android.support.v7.app.AlertDialog notArrivedDialog = new android.support.v7.app.AlertDialog.Builder(getActivity()).create();
                notArrivedDialog.setView(notArrivedDialogView);
                notArrivedDialog.setCancelable(true);
                notArrivedDialog.show();

                final EditText reasonET = (EditText) notArrivedDialogView.findViewById(R.id.reason);
                Button accept = (Button) notArrivedDialogView.findViewById(R.id.accept);
                accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (Utility.isNotNull(reasonET.getText().toString().trim())) {
                            SharedPreferences sp = getActivity().getSharedPreferences("keyData", MODE_PRIVATE);
                            final String key = sp.getString("key", "");
                            SharedPreferences preferences = getActivity().getSharedPreferences("userData", 0);
                            final String userId = preferences.getString("id", "");
                            SharedPreferences sharedPreferences = getActivity().getSharedPreferences("chatData", MODE_PRIVATE);
                            final String driverID = sharedPreferences.getString("userId", "");

                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                            String currentDateandTime = sdf.format(new Date());

                            Map<String, Object> data = new HashMap<String, Object>();
                            data.put("userID", userId);
                            data.put("driverID", driverID);
                            data.put("reason", reasonET.getText().toString().trim());
                            data.put("datetime", currentDateandTime);
                            data.put("isRejected", 0);
                            String uuid = UUID.randomUUID().toString();
                            mDatabase.child("request").child("failed").child(driverID + "-" + uuid + "-" + userId).setValue(data);
                            mDatabase.child("request").child("accepted").child(key).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    hideViews();
                                    getAllCars(false);
                                    notArrivedDialog.dismiss();
                                    drinkableR.setBackground(getResources().getDrawable(R.drawable.rounded_blue));
                                    unDrinkableR.setBackground(getResources().getDrawable(R.drawable.rounded_border));
                                }
                            });
                        } else {
                            Toast.makeText(getContext(), "من فضلك أذكر السبب", Toast.LENGTH_SHORT).show();
                        }


                    }
                });
            }
        });

        arrived.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sp = getActivity().getSharedPreferences("chatData", MODE_PRIVATE);
                final String driverID = sp.getString("userId", "");
                final String name = sp.getString("name", "");
                final String img = sp.getString("img", "");
                LayoutInflater factory = LayoutInflater.from(getActivity());
                final View arrivedDialogView = factory.inflate(R.layout.arrived, null);
                TextView nameTV = (TextView) arrivedDialogView.findViewById(R.id.name);
                Button accept = (Button) arrivedDialogView.findViewById(R.id.accept);
                ImageView driverImg = (ImageView) arrivedDialogView.findViewById(R.id.driverImg);
                RatingBar ratingBar = (RatingBar) arrivedDialogView.findViewById(R.id.rating);
                nameTV.setText(name);

                Glide.with(getContext()).load(img).into(driverImg);
                final android.support.v7.app.AlertDialog arrivedDialog = new android.support.v7.app.AlertDialog.Builder(getActivity()).create();
                arrivedDialog.setView(arrivedDialogView);
                arrivedDialog.setCancelable(true);
                arrivedDialog.show();

                accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        SharedPreferences sp = getActivity().getSharedPreferences("keyData", MODE_PRIVATE);
                        final String key = sp.getString("key", "");

                        mDatabase.child("request").child("accepted").child(key).child("status").setValue(1).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                int totalNumRate;
                                if (totalNoOfRate == 0) {
                                    totalNumRate = 1;
                                } else {
                                    totalNumRate = (Integer.valueOf(totalNoOfRate) + 1);
                                }
                                int sumOfRate = (int) (Integer.valueOf(totalRate) + rate);
                                mDatabase.child("driver").child(driverID).child("total_no_of_rate").setValue(totalNumRate);
                                mDatabase.child("driver").child(driverID).child("total_rate").setValue(sumOfRate);
                                arrivedDialog.dismiss();

                                map.clear();

                                hideViews();
                                getAllCars(false);
                                drinkableR.setBackground(getResources().getDrawable(R.drawable.rounded_blue));
                                unDrinkableR.setBackground(getResources().getDrawable(R.drawable.rounded_border));

                            }
                        });
                    }
                });

                ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                    @Override
                    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                        rate = rating;
                    }
                });
            }
        });

        chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SingleMsg.class));
            }
        });
        ivCancleNewPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                map.setOnCameraMoveListener(null);
                ivSetNewPlace.setVisibility(View.GONE);
                ivCancleNewPlace.setVisibility(View.GONE);
                myLocationUser.setVisibility(View.VISIBLE);
                getAllCars(false);

            }
        });
        ivSetNewPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getNewLocationandGetAllCar(myMarker.getPosition());
                map.setOnCameraMoveListener(null);
                ivSetNewPlace.setVisibility(View.GONE);
                ivCancleNewPlace.setVisibility(View.GONE);
                myLocationUser.setVisibility(View.VISIBLE);
            }
        });

    }

    @Override
    public void onPause() {
        super.onPause();
//        //map.setOnCameraMoveListener(null);
//        ivSetNewPlace.setVisibility(View.GONE);
//        ivCancleNewPlace.setVisibility(View.GONE);
//        myLocationUser.setVisibility(View.VISIBLE);
    }
    //====================================================================================================

    public void getCurrentDriver() {
        SharedPreferences preferences = getActivity().getSharedPreferences("userData", MODE_PRIVATE);
        final String lat = preferences.getString("lat", "");
        final String lng = preferences.getString("lng", "");
        final LatLng pp = new LatLng(Double.parseDouble(lat),Double.parseDouble(lng));
        map.clear();
        currentDriver = null;
        getMyLocation();
        SharedPreferences sp = getActivity().getSharedPreferences("chatData", MODE_PRIVATE);
        final String driverID = sp.getString("userId", "");
        //final LatLng finalPp = pp;
        mDatabase.child("driver").child(driverID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                totalNoOfRate = dataSnapshot.getValue(DriverBean.class).getTotal_no_of_rate();
                totalRate = dataSnapshot.getValue(DriverBean.class).getTotal_rate();

                if (dataSnapshot.getValue(DriverBean.class).getIsDrinkable()) {
                    markerIcon = R.mipmap.green_car;
                } else {
                    markerIcon = R.mipmap.yellow_car;
                }

                if (currentDriver == null) {

                    currentDriver = map.addMarker(new MarkerOptions().position(new LatLng(dataSnapshot.getValue(DriverBean.class).getLocation_latitude(), dataSnapshot.getValue(DriverBean.class).getLocation_longitude()))
                            .icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(getResources(), markerIcon))).title(dataSnapshot.getValue(DriverBean.class).getName())
                            .snippet(dataSnapshot.getKey() + "," + dataSnapshot.child("car_photos").getValue(DriverBean.class).getCar_img1() + "," +
                                    dataSnapshot.getValue(DriverBean.class).getMobile() + "," + dataSnapshot.getValue(DriverBean.class).getWeight() + ","
                                    + dataSnapshot.getValue(DriverBean.class).getTotal_rate() + "," + dataSnapshot.getValue(DriverBean.class).getTotal_no_of_rate() + "," + dataSnapshot.getValue(DriverBean.class).getPrice()));
                }

                MarkerAnimation.animateMarkerToICS(currentDriver, new LatLng(dataSnapshot.getValue(DriverBean.class).getLocation_latitude(),
                        dataSnapshot.getValue(DriverBean.class).getLocation_longitude()), new LatLngInterpolator.Spherical());
                currentDriver.hideInfoWindow();
                LatLng origin = pp;
                LatLng dest = new LatLng(dataSnapshot.getValue(DriverBean.class).getLocation_latitude(),
                        dataSnapshot.getValue(DriverBean.class).getLocation_longitude());

                // Getting URL to the Google Directions API
                String url = getDirectionsUrl(origin, dest);

                DownloadTask downloadTask = new DownloadTask();

                // Start downloading json data from Google Directions API
                downloadTask.execute(url);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getActivity(), databaseError+"", Toast.LENGTH_SHORT).show();
            }
        });
    }

    //============================================================================

    public static void SetLanguage(Context ctx, LANG selectedLang) {
        if (selectedLang == LANG.ENGLISH) {
            // Change layout to en
            Locale mLocale = new Locale("en");
            Locale.setDefault(mLocale);
            Configuration config = ctx.getResources().getConfiguration();

            if (!config.locale.equals(mLocale)) {
                config.locale = mLocale;
                ctx.getResources().updateConfiguration(config, null);
            }
//            SharedPrefCustom.saveLocalization(ctx, english);
        } else {
            // Change layout to ar
            Locale mLocale = new Locale("ar");
            Locale.setDefault(mLocale);
            Configuration config = ctx.getResources().getConfiguration();
            if (!config.locale.equals(mLocale)) {
                config.locale = mLocale;
                ctx.getResources().updateConfiguration(config, null);
            }
//            SharedPrefCustom.saveLocalization(ctx, arabic);
        }
    }

    private class DownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            String data = "";

            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();


            parserTask.execute(result);

        }
    }


    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList();
                lineOptions = new PolylineOptions();

                List<HashMap<String, String>> path = result.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                lineOptions.addAll(points);
                lineOptions.width(12);
                lineOptions.color(Color.RED);
                lineOptions.geodesic(true);

            }

// Drawing polyline in the Google Map for the i-th route
            if (lineOptions == null)
                return;
            map.addPolyline(lineOptions);
        }
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "mode=driving";
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.connect();

            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    public void updateMap(int capacity, String price) {
        this.capacity = capacity;
        this.price = Double.parseDouble(price);
        if (this.capacity == 0)
            getAllCars(false);
        else
            getAllCars(true);
    }

    // Convert a view to bitmap
    public Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }

    private void getMyLocation() {
        if (getActivity() != null) {
            if (myMarker != null)
                myMarker.remove();
            SharedPreferences preferences = getActivity().getSharedPreferences("userData", MODE_PRIVATE);
            final String lat = preferences.getString("lat", "");
            final String lng = preferences.getString("lng", "");
            LatLng pp = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
            myPlace = new MarkerOptions().position(pp)
                    .icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.user_location)));
            myMarker = map.addMarker(myPlace);
            myMarker.setSnippet(MYMARKER);
            map.moveCamera(CameraUpdateFactory.newLatLng(pp));

        }
    }

    public void updateMyLocation() {
        SharedPreferences preferences = getActivity().getSharedPreferences("userData", MODE_PRIVATE);
        final String userId = preferences.getString("id", "");
        final String lat = preferences.getString("lat", "");
        final String lng = preferences.getString("lng", "");
        try {
            mDatabase.child("user").child(userId).child("latitude").setValue(Double.parseDouble(lat));
            mDatabase.child("user").child(userId).child("longitude").setValue(Double.parseDouble(lng));
        }catch (Exception e){
            Toast.makeText(getActivity(), "Can't Update Your Location", Toast.LENGTH_SHORT).show();
        }
        getMyLocation();
    }
    public void getNewLocationandGetAllCar(LatLng latLng){
        try {
            SharedPreferences.Editor editor = getActivity().getSharedPreferences("userData", MODE_PRIVATE).edit();
            editor.putString("lng", latLng.longitude + "");
            editor.putString("lat", latLng.latitude + "");
            editor.commit();
            SharedPreferences preferences = getActivity().getSharedPreferences("userData", MODE_PRIVATE);
            final String userId = preferences.getString("id", "");
            mDatabase.child("user").child(userId).child("latitude").setValue(latLng.latitude);
            mDatabase.child("user").child(userId).child("longitude").setValue(latLng.longitude, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    getAllCars(false);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "Can't Update Your Location", Toast.LENGTH_SHORT).show();
        }
    }
    private void getCurrentlocation(){
        LocationManager locationManager= (LocationManager) getContext().getSystemService(LOCATION_SERVICE);
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            Location location =locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if(location==null) {
                Toast.makeText(getContext(), getString(R.string.cant_get_location), Toast.LENGTH_SHORT).show();
                return;
            }
            SharedPreferences preferences = getActivity().getSharedPreferences("userData", MODE_PRIVATE);
            final String userId = preferences.getString("id", "");
            if(!userId.isEmpty()) {
                mDatabase.child("user").child(userId).child("latitude").setValue(location.getLatitude());
                mDatabase.child("user").child(userId).child("longitude").setValue(location.getLongitude());
                SharedPreferences.Editor edit = preferences.edit();
                edit.putString("lat", String.valueOf(location.getLatitude()));
                edit.putString("lng", String.valueOf(location.getLongitude()));
                edit.commit();
            }
            LatLng pp = new LatLng(location.getLatitude(),location.getLongitude());
            myMarker.remove();
            myPlace = new MarkerOptions().position(pp)
                    .icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.user_location)));
            myMarker = map.addMarker(myPlace);
            myMarker.setSnippet(MYMARKER);
            map.moveCamera(CameraUpdateFactory.newLatLng(pp));;

        }else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 142);
        }
        /*LocationListener listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location getCurrentlocation) {
                LatLng pp = new LatLng(getCurrentlocation.getLatitude(), getCurrentlocation.getLongitude());
                Toast.makeText(getContext(), "getCurrentlocation.getLatitude() = "+getCurrentlocation.getLatitude(), Toast.LENGTH_SHORT).show();
                Toast.makeText(getContext(), "getCurrentlocation.getLongitude() = "+getCurrentlocation.getLongitude(), Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {
            }
        };
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 0, listener);
*/
    }
    public static String makePostRequest(String stringUrl, String payload) throws IOException {
        Log.e("makePostRequest: ",payload );
        URL url = new URL(stringUrl);
        HttpURLConnection uc = (HttpURLConnection) url.openConnection();
        String line;
        StringBuffer jsonString = new StringBuffer();
        uc.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded");
        uc.setRequestProperty( "charset", "utf-8");
        uc.setRequestMethod("POST");
        uc.setDoInput(true);
        uc.setInstanceFollowRedirects(false);
        uc.connect();
        OutputStreamWriter writer = new OutputStreamWriter(uc.getOutputStream(), "UTF-8");
        writer.write(payload);
        writer.close();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream()));
            while ((line = br.readLine()) != null) {
                jsonString.append(line);
            }
            br.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        uc.disconnect();
        JSONObject jObj ;
        String resutl;
        try {
            Log.e( "makePostRequest: ",jsonString.toString() );
            jObj = new JSONObject(jsonString.toString());
            resutl = jObj.getString("message");
        } catch (JSONException e) {
            e.printStackTrace();
            resutl="Sorry Message Not Send";
        }
        return resutl;
    }
}
