package com.ntamtech.waytiuser;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.crashlytics.android.Crashlytics;
import com.ntamtech.waytiuser.Activities.ChooseLanguage;
import com.ntamtech.waytiuser.Activities.Login;

import Config.SaveSharedPreference;
import io.fabric.sdk.android.Fabric;

public class Splash extends AppCompatActivity {

    Animation animation;
    Animation animationText;
    ImageView logo;
    ImageView wayti;
    ConstraintLayout constraintLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        intial();
        events();
    }

    public void intial(){
        logo = (ImageView)findViewById(R.id.logo);
        wayti = (ImageView)findViewById(R.id.wayti);
        constraintLayout = (ConstraintLayout)findViewById(R.id.container);
        animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.move);
        animationText = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.opacity);
        logo.startAnimation(animation);
        wayti.setVisibility(View.GONE);
        //constraintLayout.getBackground().setAlpha(200);
    }

    public void events(){
        animation.setAnimationListener(new Animation.AnimationListener(){
            @Override
            public void onAnimationStart(Animation arg0) {
            }
            @Override
            public void onAnimationRepeat(Animation arg0) {
            }
            @Override
            public void onAnimationEnd(Animation arg0) {
                wayti.setVisibility(View.VISIBLE);
                wayti.startAnimation(animationText);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if(!SaveSharedPreference.getUserName(Splash.this).equals("")) {
                            final Intent mainIntent = new Intent(Splash.this, Login.class);
                            startActivity(mainIntent);
                            finish();
                        }else {
                            final Intent mainIntent = new Intent(Splash.this, ChooseLanguage.class);
                            startActivity(mainIntent);
                            finish();
                        }
                    }
                }, 1000);
            }
        });
    }

}
